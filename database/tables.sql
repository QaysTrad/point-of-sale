CREATE DATABASE cash;

USE cash;

CREATE TABLE cashier (
  userId int NOT NULL AUTO_INCREMENT,
  fullName varchar(20) NOT NULL,
  email varchar(20) NOT NULL,
  PRIMARY KEY (userId) 
);

CREATE TABLE transaction (
  transId int NOT NULL AUTO_INCREMENT,
  userId int,
  email varchar(20) NOT NULL,
  PRIMARY KEY (transId),
  FOREIGN KEY (userId) REFERENCES cashier(userId)
);

CREATE TABLE logs(
	logId int NOT NULL AUTO_INCREMENT,
	userId int,
	cost int NOT NULL,
	paidValue int NOT NULL,
	PRIMARY KEY (logId),
	FOREIGN KEY (userId) REFERENCES cashier(userId)
);

