import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { OwnerComponent } from './owner/owner.component';
import { CashierComponent } from './cashier/cashier.component';


const appRoutes: Routes = [
  { path: '',
    component: HomeComponent
  },
  { path: 'login',
    component: LoginComponent
  },
  { path: 'owner',
    component: OwnerComponent
  },
  { path: 'cashier',
    component: CashierComponent
  },

]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    OwnerComponent,
    CashierComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule
  ],
   exports: [
   BrowserModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})


export class AppModule { }
